package br.com.matriz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Principal {

public static void main(String[] args) {
		
		int vetor[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		   
		int matriz[][] = {
							{1,1,1,2,2,3,4,5,13,14,15},
							{1,1,2,2,2,3,10,11,4,5,6,7},
							{1,1,1,2,1,5,6,9,9,9,15,12,15,15}
						};
		    
	    Object[] itens = contaItensDoVetorNaMatiz(matriz,vetor);
	    
	    String str = Arrays.deepToString(itens);
	    
		System.out.println(str);
	}

	
	public static Object[] contaItensDoVetorNaMatiz(int matriz[][], int vetor[])  
	{  
		List<String> list = new ArrayList<String>();
		int[] res= new int[15];
		for (int i = 0; i < vetor.length; i++) {
			for (int m = 0; m < matriz.length; m++) {
				for (int n = 0; n < matriz[m].length; n++) {
					if(matriz[m][n] == vetor[i] ) {
						res[i] ++;
					}
				}  
			}
			
			list.add("["+vetor[i]+"] = "+ res[i]);
		}
		
		return list.toArray();		
	}
	

}
